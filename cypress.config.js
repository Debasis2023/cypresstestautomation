const { defineConfig } = require("cypress");
const getspecFiles = require("cypress-gitlab-parallel-runner")

module.exports = defineConfig({
  include: ["./node_modules/cypress", "cypress/**/*.js"],
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      getspecFiles("./cypress/e2e",true)
    },
    experimentalStudio:true,
    experimentalWebKitSupport:true,
    video:false
  },
});
