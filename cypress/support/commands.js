// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('Login',(email,pass) => {
    cy.get('#Email').clear().type(email)
    cy.get('#Password').clear().type(pass)
    cy.get('.login-button').contains('Log in').click()
})

Cypress.Commands.add('Logout',() => {
    cy.contains('Logout').click()
})
Cypress.Commands.add('verifyText',(text) => {
    cy.get('div>h4').first().should('contain.text',text)
})

Cypress.Commands.add('getFrame',(locator,text) => {
    cy.get(locator).within(($iframe) => {
    const frame = $iframe.contents().find('#tinymce p')
    cy.wrap(frame).type(text)
    })
})
