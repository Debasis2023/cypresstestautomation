import users from '../fixtures/example.json'

describe('Assertion Demo',() =>{
    beforeEach(() =>{
        cy.visit('https://admin-demo.nopcommerce.com/')
        cy.viewport(1920,1080)
        cy.Login('admin@yourstore.com','admin')
    })
    afterEach(() =>{
        cy.Logout()
    })
    it.skip('verify text content check',() => {
        //1st approach
        cy.get('.content-header h1').should('be.visible').and('contain.text','Dashboard')

        //2nd approach
        cy.get('.content-header h1').invoke('text').should('contain','Dashboard')

        //3rd approach
        cy.get('.content-header h1').then((text) => {
            expect(text.text().trim()).to.be.equal('Dashboard')
        })

        //4th approach
        cy.get('.content-header h1').invoke('text').then(text => {
            expect(text.trim()).to.be.equal('Dashboard')
        })

        //5th approach
        cy.get('.content-header h1').then((h) => h.text().trim()).should('equal','Dashboard')
    })

    it.only('Verify the colum headers in table',() => {
        cy.get('ul.nav-pills>li:nth-child(2)>a>p').click()
        cy.get('ul.nav-treeview>li:nth-child(1)>a[href*="Product/List"]>p').click({force:true})
        // cy.get('.dataTables_scrollHeadInner table thead th').each((item,index,list) => {
        //     if(index>0){
        //         cy.get(item).invoke('text').should('be.oneOf',['Picture','Product name','SKU','Price','Stock quantity','Published','Edit'])
        //     }
        // })

        let headers = users.tabHeaders
        cy.get('.dataTables_scrollHeadInner table thead th').each((item,index,list) => {
            cy.get(item).invoke('text').then((text) => {
                expect(text).to.equal(headers[index])
            })
        })
    })
})