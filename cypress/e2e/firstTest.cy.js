describe('First Test', () => {
  it('login to admin', () => {
    cy.visit('https://admin-demo.nopcommerce.com/')
    cy.viewport(1920,1080)
    cy.get('#Email').clear().type('admin@yourstore.com')
    cy.get('#Password').clear().type('admin')
    cy.get('.login-button').contains('Log in').click()

    const menuList = ['Dashboard','Catalog','Sales','Customers','Promotions','Content management','Configuration','System','Reports','Help']
    //Get all Menu list
    cy.get('ul[class*="nav-pills"] >li> a> p').as('MenuList')
    cy.get('@MenuList').each((item,index,list) => {
      //first way 
      //cy.wrap(item).should('contain.text',menuList[index])

      //2nd way
      //expect(Cypress.$(item).text().trim()).eq(menuList[index])
      if(index>1){
        cy.get(item).click()
        //cy.get('li[class*="menu-open"]>ul>li').as('SubMenuList')
      }
    })
    
  })
})