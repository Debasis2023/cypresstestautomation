import menus from '../fixtures/data.json'
describe.skip('Test with multiple data set',() => {
    
    Object.keys(menus).forEach(key => {
        it.skip('Verify Menus',() => {
            cy.visit('https://admin-demo.nopcommerce.com/')
            cy.viewport(1920,1080)
            cy.Login('admin@yourstore.com','admin')
            cy.verifyText('Welcome to your store!')
            let xpath = "//p[normalize-space()='"+key+"']"
            cy.xpath(xpath).click()
            
            const subMenu = new Array()
            cy.get('ul[class*="nav-pills"] >li> a> p').as('MenuList')
            cy.get('@MenuList').each((item,index,list) => {
                expect(Cypress.$(item).text().trim()).eq(menuList[index])
                subMenu.push(Cypress.$(item)).text().trim()
            })
        })
    })
})