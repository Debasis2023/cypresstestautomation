describe('Enter text into input box',() => {
    beforeEach(() => {
        cy.visit('https://admin-demo.nopcommerce.com')
        cy.viewport(1920,1080)
        cy.Login('admin@yourstore.com','admin')
    })
    afterEach(() => {
        //cy.Logout()
    })
    it('enter values in text box',() => {
        cy.get('ul.nav-pills>li:nth-child(2)>a>p').click()
        cy.get('ul.nav-treeview>li:nth-child(2)>a[href*="Category/List"]>p').click({force:true})
        cy.get('a[href*="Category/Create"]').click()
        cy.get('#Name').type('Testing in cypress')
        cy.getFrame('#Description_ifr',"Hello Cypress") 
        /*cy.get('#Description_ifr').within(($iframe) => {
            const frame = $iframe.contents().find('#tinymce p')
            cy.wrap(frame).type('Hello Debasis')
        })*/
    })
})